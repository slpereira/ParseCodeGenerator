package com.follow.domesticafacil.model;

import com.parse.ParseObject;

import org.json.JSONObject;

/**
 * Created by Silvio on 17/06/2015.
 */
public class BaseModel extends ParseObject {

    protected void setValue(String key, Object value) {
        put(key, value != null ? value : JSONObject.NULL);
    }

}
