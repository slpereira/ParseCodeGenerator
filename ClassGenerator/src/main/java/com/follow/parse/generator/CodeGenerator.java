package com.follow.parse.generator;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.follow.domesticafacil.model.BaseModel;
import com.google.gson.internal.LinkedTreeMap;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JTypeVar;
import com.sun.codemodel.JVar;

public class CodeGenerator {

	private JCodeModel codeModel;
	
	public static String capitalize(String input) {
		return input.substring(0, 1).toUpperCase() + input.substring(1);
	}
	
	public void generateClass(String fullClassName, Map schema, List<String> exclude) throws JClassAlreadyExistsException, IOException, ClassNotFoundException {
		codeModel = new JCodeModel();
		JDefinedClass dc = codeModel._class(fullClassName)._extends(BaseModel.class);
		dc.annotate(ParseClassName.class).param("value", dc.name());
		LinkedTreeMap map = (LinkedTreeMap) schema.get("fields");
		Iterator<Entry<String, LinkedTreeMap>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			 Entry<String, LinkedTreeMap> field = iterator.next();
			 if (field.getValue().get("type").equals("ACL") || exclude != null && exclude.contains(field.getKey()))
				 continue;
			 // declare the static field
			 JFieldVar fVar = dc.field(JMod.FINAL | JMod.STATIC | JMod.PUBLIC, String.class, field.getKey().toUpperCase());
			 fVar.init(JExpr.lit(field.getKey()));
			 // we need to introspect the field value
			 LinkedTreeMap fData = field.getValue();
			 Iterator<Entry<String, String>> fDataIterator = fData.entrySet().iterator();
			 String type = (String) fData.get("type");
			 if (type.equalsIgnoreCase("Pointer")) {
				 JMethod mGet = dc.method(JMod.PUBLIC, ParseObject.class, "get" + capitalize(field.getKey()));
				 JBlock blGet = mGet.body();
				 blGet._return( JExpr.invoke("getParseObject").arg(fVar) );
				 
				 JMethod mSet = dc.method(JMod.PUBLIC, codeModel.VOID, "set" + capitalize(field.getKey()));
				 JVar vParam = mSet.param(ParseObject.class, "value");
				 JBlock blSet = mSet.body();
				 blSet.invoke("setValue").arg(fVar).arg(vParam);
			 }
			 else if (type.equals("ACL")) {
				 String javaType = "com.parse.ParseACL";
				 JMethod mGet = dc.method(JMod.PUBLIC, codeModel.parseType(javaType), "get" + capitalize(field.getKey()));
				 JBlock blGet = mGet.body();
				 JExpression exp = JExpr._super();
				 blGet._return( exp.invoke("get" + type) );
				 
				 JMethod mSet = dc.method(JMod.PUBLIC, codeModel.VOID, "set" + capitalize(field.getKey()));
				 JVar vParam = mSet.param( codeModel.parseType(javaType), "value");
				 JBlock blSet = mSet.body();
				 blSet.add(exp.invoke("setACL").arg(vParam));
			 }
			 else {
				 String javaType = type;
				 if (type.equalsIgnoreCase("Date"))
					 javaType = "java.util.Date";
				 JMethod mGet = dc.method(JMod.PUBLIC, codeModel.parseType(javaType), "get" + capitalize(field.getKey()));
				 JBlock blGet = mGet.body();
				 blGet._return( JExpr.invoke("get" + type).arg(fVar) );
				 
				 JMethod mSet = dc.method(JMod.PUBLIC, codeModel.VOID, "set" + capitalize(field.getKey()));
				 JVar vParam = mSet.param( codeModel.parseType(javaType), "value");
				 JBlock blSet = mSet.body();
				 blSet.invoke("setValue").arg(fVar).arg(vParam);
			 }
		}
		
		codeModel.build(new File("C:\\Temp\\"));
		
		
	}
	
}
