package com.follow.parse.generator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

public class Schema {
	
	private static final String APP_KEY = "<YOUR_API_KEY>";
	private static final String MASTER_KEY = "<YOUR_MASTER_KEY>";
	
	private static final String URL = "https://api.parse.com/1/schemas/";
	
	public Map loadSchema(String document) throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(URL.concat(document));
		httpget.addHeader("X-Parse-Application-Id", APP_KEY);
		httpget.addHeader("X-Parse-Master-Key", MASTER_KEY);
		CloseableHttpResponse response = httpclient.execute(httpget);
		try {
		    HttpEntity entity = response.getEntity();
		    Map map=new HashMap<>();
		    map = new Gson().fromJson(EntityUtils.toString(entity), map.getClass());
		    return map;
		} finally {
		    response.close();
		}
	}

}