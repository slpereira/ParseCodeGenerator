package com.follow.parse.generator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.junit.Assert;
import org.junit.Test;

import com.sun.codemodel.JClassAlreadyExistsException;

public class CodeGeneratorTest {

	@Test
	public void test() throws ClientProtocolException, IOException, JClassAlreadyExistsException, ClassNotFoundException {
		Schema schema = new Schema();
		Map m = schema.loadSchema("TipoLancamento");
	    Assert.assertTrue(m.size() > 0);
	    System.out.println(m);
	    CodeGenerator cg = new CodeGenerator();
	    List<String> exclude = new ArrayList<>();
	    exclude.add("objectId");
	    exclude.add("createdAt");
	    exclude.add("updatedAt");
	    cg.generateClass("com.follow.domesticafacil.model.TipoLancamento", m, exclude);
	}

}
